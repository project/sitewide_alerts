# Site-wide Alerts

This module allows you to manage and display site alerts.

Module provides custom site_alert entity to store alert
information.

Site alert entities can be translated as needed and also
ordered in terms of display priority.

Submodule provided for hooking into Admin audit trail
module.

Site alerts can also be sent to Twitter as a tweet/status
update.

Custom blocks also provided for rendering site alerts under
a different view mode and for display the different alert
types (taxonomy terms).

## Dependencies

- Drupal 9.3+ or 10.

Drupal versions 9.2 or older, including Drupal 8 are no longer supported.
The 1.x branch exists for those cases but is no longer maintained and may 
be removed at any time.

### Required modules

#### Drupal core modules:

* User
* Media
* Content translation
* Language
* Link

#### Contrib modules:

* Color field (https://www.drupal.org/project/color_field)
* Field group (https://www.drupal.org/project/field_group)
* Key (https://www.drupal.org/project/key)
* Link attributes (https://www.drupal.org/project/link_attributes)
* Maxlength (https://www.drupal.org/project/maxlength)
* Weight (https://www.drupal.org/project/weight)

#### Optional/sub modules:

* Admin Audit Trail (https://www.drupal.org/project/admin_audit_trail)

#### Third party libraries
* TwitterOauth (https://twitteroauth.com)

### Migration from 1.x to 2.x

Unfortunately, no migration path is available from 1.x to 2.x.

You will need to uninstall the existing sitewide_alerts module.

```bash
drush pmu sitewide_alerts
```

Next, you will need to update the module to ^2.0.

```bash
composer update drupal/sitewide_alerts:^2.0
```

Once module has been updated, you can re-enable the module as
such:

```bash
drush en sitewide_alerts
```

You will need to re-configure settings for the module and re-create
any site alerts similar to before.

### Installation/Setup

1. Install and enable module. Use composer method.

```bash
composer require drupal/sitewide_alerts:^2.0
```

```bash
drush en sitewide_alerts
```

2. Settings can be configured at `(/admin/config/site-alert)`.

3. Twitter settings can be configured at
`(/admin/config/site-alert/twitter)`.

4. Additional fields for site entity can be managed at
`(/admin/structure/site-alert/fields)`.

5. Site alert field display and view modes can be managed at
`(/admin/structure/site-alert/display)`.

6. You can see an overview of site alerts at
`(/admin/content/site-alert)`. You can also create new site alerts
by clicking on "Add site alert" button.

7. Existing site alerts can be ordered at
`(/admin/content/site-alert/order)`.

8. Additional view modes can be configured for site alert at
`(/admin/structure/display-modes/view)`.

### Admin Audit Trail - Site Alert

Module provides optional submodule to audit/track site alert
entity changes. This module requires the admin_audit_trail module.

The submodule will track/insert entry when a site alert is created,
updated, and also deleted.

To enable, Site alert module must be installed/enabled prior to.

See below for drush command to enable submodule:

```bash
drush en admin_audit_trail_sitewide_alerts
```

For more information, see
https://www.drupal.org/project/admin_audit_trail.

### Twitter Integration

Module provides additional functionality to post status updates
directly on a users' timeline/page, if authorized.

Currently, works with Twitter API _v1.1_ & _v2_. v2 is
recommended and is the default.

Requires Twitter account and the following API keys/tokens for
Twitter app:
* API key
* API key secret
* Access token
* Access token secret

API keys/tokens can be found under the app on the Twitter
developer dashboard:
https://developer.twitter.com/en/portal/dashboard

For the app user authentication settings, OAuth 1.0a should
be turned on. Read and write permissions are required.

Callback URI/Url should look something like the following:

```bash
https://yourdomain.com/twitter/oauth
```

_/twitter/oauth_ is the endpoint that Twitter will ping on the
site.

Website URL should just be the domain part:

```bash
https://yourdomain.com
```

Once your app is setup, you will need to configure Twitter
settings at `(/admin/config/site-alert/twitter)`. The settings
are per language. If using multiple languages, you will need to
authorize an account per language. You can also use two apps or
the one per language.

Once the credentials key is configured and setup. You can begin
the authorization process. This process can be started at
`(/admin/config/site-alert/twitter/authorize)`. You should be
logged into the Twitter account you wish to authorize. Once
authorized, the tokens will be stored and used going forward
when tweeting on the configured language.

See below for more information around the credentials file.

For more Twitter developer information, please visit:
https://developer.twitter.com

#### Credentials file

This module leverages the _key_ module to link credentials file and
store keys securely. You will need to create a file that lives
outside of web root directory.

Filename: _../twitter_credentials.key_

Example of what the inside of the file should look like:

```ini
version=2
key=YOUR_VALUE_HERE
key_secret=YOUR_VALUE_HERE
access_token=YOUR_VALUE_HERE
access_token_secret=YOUR_VALUE_HERE
```
If using v1.1, just set version to _1.1_ instead of _2_. Using
version 2 is recommended and is the default.

The above information can be found on the developer portal,
under your app settings.

#### Keys

Keys can be managed here: `(admin/config/system/keys)`

For more information on the Key module, see
https://www.drupal.org/project/key

### Custom Blocks

See below for the blocks provided with this module.

#### Site alerts block

This block displays site alert entities and allows you to
select which view mode you would like to use. This will allow
you to configure other view modes and display them as such.

The site alerts displayed here will still respect the order/weight
configured per site alert.

#### Site alert types block

This block displays the alert types' taxonomy terms and their
respective fields. View mode can also be configured here as well.
Useful for printing out a legend or to let the user know
what types are available.

### Taxonomy

See below for taxonomy provided with this module.

#### Alert types

The module provides an "Alert types" vocabulary for storing the
associated alert type, it's icon, color, and whether the alert type
is dismissible or not. The dismissible option can be configured on
the alert type level and also at the alert level. Alert type level
will override alert level.

Site alert types can be managed at
`(admin/structure/taxonomy/manage/site_alert_types/overview)`

### Theming/template suggestions

The following theme and templates are provided by module. See
examples below on what templates can be overridden in your own
theme.

Simply grab the base template from the module and place in your
own theme templates folder.

For more information, just enable twig debug.

#### sitewide_alerts/templates/site-alerts.html.twig

Wrapper template used for displaying multiple site alert entities.

View mode can also be used in template suggestion.

```html
<!-- THEME HOOK: 'site_alerts' -->
<!-- FILE NAME SUGGESTIONS:
   * site-alerts--alert-bar.html.twig
   * site-alerts.html.twig
-->
```

#### sitewide_alerts/templates/site-alert.html.twig

Template used for displaying site alert entity.

Site alert id and view mode are also used in template suggestion.

```html
<!-- THEME HOOK: 'site_alert' -->
<!-- FILE NAME SUGGESTIONS:
   * site-alert--1--alert-bar.html.twig
   * site-alert--1.html.twig
   * site-alert--alert-bar.html.twig
   * site-alert.html.twig
-->
```

#### sitewide_alerts/templates/site-alert-close.html.twig

Template used for displaying close/dismiss button.

View mode can also be used in template suggestion.

```html
<!-- THEME HOOK: 'site_alert_close' -->
<!-- FILE NAME SUGGESTIONS:
   * site-alert-close--alert-bar.html.twig
   * site-alert-close.html.twig
-->
```

#### sitewide_alerts/templates/site-alert-type.html.twig

Template used for displaying site alert type.

View mode can also be used in template suggestion.

```html
<!-- THEME DEBUG -->
<!-- THEME HOOK: 'site_alert_type' -->
<!-- FILE NAME SUGGESTIONS:
   * site-alert-type--alert-bar.html.twig
   * site-alert-type.html.twig
-->
```
