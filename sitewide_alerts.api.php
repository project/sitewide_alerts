<?php

/**
 * @file
 * Hooks specific to the Site alert module.
 */

use Drupal\Core\Database\Query\Select;

/**
 * @addtogroup hooks
 */

/**
 * Alter site alerts query.
 *
 * This hook is invoked from the getSiteAlerts method defined
 * in the SiteAlertService class.
 *
 * @param \Drupal\Core\Database\Query\Select $query
 *   The query to be altered or added to.
 */
function hook_sitewide_alerts_alter_query(Select &$query): void {
}

/**
 * Alter site alert entities.
 *
 * This hook is invoked from the getSiteAlerts method defined
 * in the SiteAlertService class.
 *
 * Site alerts from this method can be altered prior to
 * being returned.
 *
 * @param array $site_alerts
 *   The array of site alert entities to alter.
 */
function hook_sitewide_alerts_alter_site_alerts(array &$site_alerts): void {
}
