<?php

/**
 * @file
 * Theme for sitewide_alerts.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for site alerts template.
 *
 * Default template: site-alerts.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - alerts: The site alert object/entities.
 *   - view_mode: View mode; e.g., 'full', 'teaser', etc.
 *
 * @see hook_entity_type_build()
 * @see \Drupal\Core\Field\BaseFieldDefinition::setDisplayConfigurable()
 */
function template_preprocess_site_alerts(array &$variables): void {
  $view_mode = $variables['view_mode'];

  $variables['attributes'] = new Attribute();
  $variables['attributes']->addClass('c-site-alerts');

  if (!empty($view_mode)) {
    $variables['attributes']->addClass('view-mode__' . Html::getClass($view_mode));
  }
}

/**
 * Prepares variables for site alert types template.
 *
 * Default template: site-alert-types.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - alert_types: The site alert type object/entities.
 *   - view_mode: View mode; e.g., 'full', 'teaser', etc.
 *
 * @see hook_entity_type_build()
 * @see \Drupal\Core\Field\BaseFieldDefinition::setDisplayConfigurable()
 */
function template_preprocess_site_alert_types(array &$variables): void {
  $view_mode = $variables['view_mode'];

  $variables['attributes'] = new Attribute();
  $variables['attributes']->addClass('c-site-alert-types');

  if (!empty($view_mode)) {
    $variables['attributes']->addClass('view-mode__' . Html::getClass($view_mode));
  }
}

/**
 * Prepares variables for site alert template.
 *
 * Default template: site-alert.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An array of elements to display in view mode.
 *   - site_alert: The site alert object/entity.
 *   - view_mode: View mode; e.g., 'full', 'teaser', etc.
 *
 * @see hook_entity_type_build()
 * @see \Drupal\Core\Field\BaseFieldDefinition::setDisplayConfigurable()
 */
function template_preprocess_site_alert(array &$variables): void {
  $view_mode = $variables['view_mode'] = $variables['elements']['#view_mode'];

  /** @var \Drupal\sitewide_alerts\SiteAlertInterface $site_alert */
  $variables['site_alert'] = $variables['elements']['#site_alert'];
  $site_alert = $variables['site_alert'];

  // Set site alert type.
  $site_alert_type = $site_alert->getAlertType();
  $variables['site_alert_type'] = $site_alert_type;
  $variables['site_alert_type_icon'] = [
    '#theme' => 'site_alert_type',
    '#site_alert_type_icon' => $site_alert->getAlertTypeIcon(),
    '#site_alert_type' => $site_alert_type,
    '#view_mode' => $view_mode,
  ];

  // Set site alert close button.
  $variables['site_alert_close'] = NULL;
  if ($view_mode == 'alert_bar') {
    $variables['site_alert_close'] = [
      '#theme' => 'site_alert_close',
      '#site_alert' => $site_alert,
      '#view_mode' => $view_mode,
    ];
  }

  // Helpful $content variable for templates.
  $variables += ['content' => []];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Add alert content classes.
  $variables['content_attributes']['class'][] = 'c-site-alert__content';

  // Add alert classes.
  $variables['attributes']['class'][] = 'c-site-alert';
  $variables['attributes']['class'][] = 'view-mode__' . Html::getClass($view_mode);

  // Add alert type label as a class.
  $variables['attributes']['class'][] = 'alert-type__' . Html::getClass($site_alert_type->label());

  // Add alert type color.
  $variables['site_alert_type_color'] = $site_alert->getAlertTypeColor();
  if (!empty($variables['site_alert_type_color']) && $view_mode == 'alert_bar') {
    $variables['attributes']['style'] = 'background-color:' . $variables['site_alert_type_color'];
  }

  // Add alert ARIA role.
  $variables['attributes']['role'] = 'alert';

  // Add data dismiss key.
  if ($view_mode == 'alert_bar') {
    $dismiss_key = $site_alert->id() . '-' . $site_alert->language()->getId();
    $variables['attributes']['data-dismiss-key'] = $dismiss_key;
  }
}

/**
 * Prepares variables for site alert type template.
 *
 * Default template: site-alert-type.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *     - site_alert_type: The site alert type taxonomy term entity.
 *     - site_alert_type_icon: The site alert type icon.
 *        .url: The path/url to icon.
 *        .alt: The alt text for the icon.
 *     - view_mode: View mode; e.g., 'full', 'teaser', etc.
 *
 * @see hook_entity_type_build()
 * @see \Drupal\Core\Field\BaseFieldDefinition::setDisplayConfigurable()
 */
function template_preprocess_site_alert_type(array &$variables): void {
  $view_mode = $variables['view_mode'];

  $variables['attributes'] = new Attribute();
  $variables['attributes']->addClass('c-site-alert-type');

  if (!empty($view_mode)) {
    $variables['attributes']->addClass('view-mode__' . Html::getClass($view_mode));
  }
}
