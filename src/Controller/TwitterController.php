<?php

namespace Drupal\sitewide_alerts\Controller;

use Abraham\TwitterOAuth\TwitterOAuth;
use Abraham\TwitterOAuth\TwitterOAuthException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\key\KeyRepository;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Twitter controller.
 *
 * @package Drupal\sitewide_alerts\Controller
 */
class TwitterController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * The language manager.
   */
  protected $languageManager;

  /**
   * The current language code.
   */
  protected string $language;

  /**
   * The key repository.
   */
  protected KeyRepository $keyRepository;

  /**
   * The constructor.
   *
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\key\KeyRepository $key_repository
   *   The key repository.
   */
  public function __construct(
    SiteAlertService $site_alert_service,
    LanguageManager $language_manager,
    KeyRepository $key_repository
  ) {
    $this->siteAlertService = $site_alert_service;
    $this->languageManager = $language_manager;
    $this->language = $language_manager->getCurrentLanguage()->getId();
    $this->keyRepository = $key_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sitewide_alerts.site_alert_service'),
      $container->get('language_manager'),
      $container->get('key.repository')
    );
  }

  /**
   * Mask token.
   *
   * @param string $token
   *   The token.
   *
   * @return string
   *   The masked token.
   */
  private function maskToken(string $token): string {
    return substr_replace($token, str_repeat('*', strlen($token) - 4), 0, -4);
  }

  /**
   * Twitter Oauth Callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function oauthCallback(Request $request): array {
    $build = [];

    // Get current language and associated twitter configuration.
    $state_keys = [
      'twitter_key_name.' . $this->language,
    ];
    $config = $this->siteAlertService->getStateConfig($state_keys);

    // Get twitter key name.
    $twitter_key_name = $config['twitter_key_name.' . $this->language] ?? NULL;
    if (!empty($twitter_key_name)) {
      // Get key and credentials.
      $twitter_credentials = parse_ini_string($this->keyRepository->getKey($twitter_key_name)
        ->getKeyValue());

      // Set twitter configuration vars.
      $consumer_key = $twitter_credentials['key'] ?? NULL;
      $consumer_secret = $twitter_credentials['key_secret'] ?? NULL;
      $access_token = $twitter_credentials['access_token'] ?? NULL;
      $access_token_secret = $twitter_credentials['access_token_secret'] ?? NULL;
      $oauth_verifier = $request->query->get('oauth_verifier');
      $oauth_token = $request->query->get('oauth_token');

      if (!empty($oauth_verifier) && !empty($oauth_token)) {
        try {
          $connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
          $auth_response = $connection->oauth("oauth/access_token", [
            "oauth_token" => $oauth_token,
            "oauth_verifier" => $oauth_verifier,
          ]);

          $access_token = $auth_response['oauth_token'];
          $secret_token = $auth_response['oauth_token_secret'];
          if (!empty($access_token) && !empty($secret_token)) {
            $state_data = [
              'twitter_access_token.' . $this->language => $access_token,
              'twitter_secret_token.' . $this->language => $secret_token,
            ];
            $this->siteAlertService->setStateConfig($state_data);

            $this->messenger()
              ->addMessage($this->t('Twitter authorization was successful.'));

            $build = [
              'access_token' => [
                '#markup' => '<p>' . $this->t('Access token: @access_token.', ['@access_token' => $this->maskToken($access_token)]) . '</p>',
              ],
              'secret_token' => [
                '#markup' => '<p>' . $this->t('Secret token: @secret_token.', ['@secret_token' => $this->maskToken($secret_token)]) . '</p>',
              ],
            ];
          }
          else {
            $this->messenger()
              ->addError($this->t('Twitter callback failed. Unable to find or set tokens.'));
          }
        }
        catch (TwitterOAuthException $exception) {
          watchdog_exception('sitewide_alerts', $exception);
          $this->messenger()->addError($exception->getMessage());
        }
      }
    }

    return $build;
  }

}
