<?php

namespace Drupal\sitewide_alerts\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\sitewide_alerts\SiteAlertInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the site alert entity class.
 *
 * @ContentEntityType(
 *   id = "site_alert",
 *   label = @Translation("Site alert"),
 *   label_collection = @Translation("Site alerts"),
 *   label_singular = @Translation("site alert"),
 *   label_plural = @Translation("site alerts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count site alerts",
 *     plural = "@count site alerts",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\sitewide_alerts\SiteAlertStorage",
 *     "list_builder" = "Drupal\sitewide_alerts\SiteAlertListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\sitewide_alerts\SiteAlertAccessControlHandler",
 *     "translation" = "Drupal\sitewide_alerts\SiteAlertTranslationHandler",
 *     "form" = {
 *       "add" = "Drupal\sitewide_alerts\Form\SiteAlertEntityForm",
 *       "edit" = "Drupal\sitewide_alerts\Form\SiteAlertEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sitewide_alerts\Routing\SiteAlertHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "site_alert",
 *   data_table = "site_alert_field_data",
 *   revision_table = "site_alert_revision",
 *   revision_data_table = "site_alert_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer site alert",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/site-alert",
 *     "add-form" = "/admin/content/site-alert/add",
 *     "canonical" = "/admin/content/site-alert/{site_alert}",
 *     "edit-form" = "/admin/content/site-alert/{site_alert}/edit",
 *     "preview-form" = "/admin/content/site-alert/{site_alert}/preview",
 *     "version-history" = "/admin/content/site-alert/{site_alert}/revisions",
 *     "revision" = "/admin/content/site-alert/{site_alert}/revisions/{site_alert_revision}/view",
 *     "revision-revert" = "/admin/content/site-alert/{site_alert}/revisions/{site_alert_revision}/revert",
 *     "translation-revert" = "/admin/content/site-alert/{site_alert}/revisions/{site_alert_revision}/revert/{langcode}",
 *     "revision-delete" = "/admin/content/site-alert/{site_alert}/revisions/{site_alert_revision}/delete",
 *     "tweet-form" = "/admin/content/site-alert/{site_alert}/tweet",
 *     "delete-form" = "/admin/content/site-alert/{site_alert}/delete",
 *   },
 *   field_ui_base_route = "entity.site_alert.settings",
 * )
 */
class SiteAlert extends RevisionableContentEntityBase implements SiteAlertInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the node owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSaveRevision(EntityStorageInterface $storage, \stdClass $record) {
    parent::preSaveRevision($storage, $record);

    if (!$this->isNewRevision() && isset($this->original) && (!isset($record->revision_log) || $record->revision_log === '')) {
      // If we are updating an existing node without adding a new revision, we
      // need to make sure $entity->revision_log is reset whenever it is empty.
      // Therefore, this code allows us to avoid clobbering an existing log
      // entry with an empty one.
      $record->revision_log = $this->original->revision_log->value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the site alert was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the site alert was last edited.'));

    $fields['weight'] = BaseFieldDefinition::create('weight')
      ->setLabel(t('Weight'))
      ->setTranslatable(TRUE)
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'weight_selector',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): ?int {
    $weight = 0;
    if ($this->hasField('weight') && !$this->get('weight')->isEmpty()) {
      $weight = $this->get('weight')->value;
    }
    return $weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlertType(): ?EntityInterface {
    $alert_type = NULL;
    if ($this->hasField('field_site_alert_type') && !$this->get('field_site_alert_type')->isEmpty()) {
      $id = $this->get('field_site_alert_type')->getValue()[0]['target_id'];
      $language = $this->languageManager()->getCurrentLanguage()->getId();
      /** @var \Drupal\taxonomy\Entity\Term $alert_type */
      $alert_type = $this->entityTypeManager()->getStorage('taxonomy_term')->load($id);
      if ($alert_type->isTranslatable() && $alert_type->hasTranslation($language)) {
        $alert_type = $alert_type->getTranslation($language);
      }
    }
    return $alert_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlertTypeIcon(): array {
    $alert_type_icon = [
      'url' => '',
      'alt' => '',
      'title' => '',
    ];
    /** @var \Drupal\taxonomy\Entity\Term $alert_type */
    if ($alert_type = $this->getAlertType()) {
      if ($alert_type->hasField('field_site_alert_type_icon') && !$alert_type->get('field_site_alert_type_icon')->isEmpty()) {
        $media_id = $alert_type->get('field_site_alert_type_icon')->getValue()[0]['target_id'];
        $media = Media::load($media_id);
        $source = $media->getSource();
        $source_config = $source->getConfiguration();
        if (!empty($source_config['source_field'])) {
          $source_field = $source_config['source_field'];
          if ($media->hasField($source_field) && !$media->get($source_field)->isEmpty()) {
            $alert_type_icon['alt'] = $media->get($source_field)->getValue()[0]['alt'];
            $alert_type_icon['title'] = $media->get($source_field)->getValue()[0]['title'];
          }
        }
        if ($fid = $source->getSourceFieldValue($media)) {
          $file = File::load($fid);
          $alert_type_icon['url'] = $file->createFileUrl(FALSE);
        }
      }
    }
    return $alert_type_icon;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlertTypeColor(): string {
    $alert_type_color = '';
    /** @var \Drupal\taxonomy\Entity\Term $alert_type */
    if ($alert_type = $this->getAlertType()) {
      if ($alert_type->hasField('field_site_alert_type_color') && !$alert_type->get('field_site_alert_type_color')->isEmpty()) {
        $alert_type_color = $alert_type->get('field_site_alert_type_color')->getValue()[0]['color'];
      }
    }
    return $alert_type_color;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlertLink(): string {
    $alert_link = '';
    if ($this->hasField('field_site_alert_link') && !$this->get('field_site_alert_link')->isEmpty()) {
      $uri = $this->get('field_site_alert_link')->getValue()[0]['uri'];
      if ($url = Url::fromUri($uri)) {
        $options = [
          'absolute' => TRUE,
          'language' => $this->languageManager()->getCurrentLanguage(),
        ];
        $alert_link = $url->setOptions($options)->toString();
      }
    }
    return $alert_link;
  }

  /**
   * {@inheritdoc}
   */
  public function isDismissible(): bool {
    $dismissible = FALSE;

    if ($this->hasField('field_site_alert_dismissible') && !$this->get('field_site_alert_dismissible')->isEmpty()) {
      if ($this->get('field_site_alert_dismissible')->getValue()[0]['value'] == 1) {
        $dismissible = TRUE;
      }
    }

    /** @var \Drupal\taxonomy\Entity\Term $alert_type */
    if ($alert_type = $this->getAlertType()) {
      if ($alert_type->hasField('field_site_alert_type_dismiss') && !$alert_type->get('field_site_alert_type_dismiss')->isEmpty()) {
        if ($alert_type->get('field_site_alert_type_dismiss')->getValue()[0]['value'] == 1) {
          $dismissible = TRUE;
        }
        else {
          $dismissible = FALSE;
        }
      }
    }

    return $dismissible;
  }

}
