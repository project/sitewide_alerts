<?php

namespace Drupal\sitewide_alerts\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the site alert entity edit forms.
 */
class SiteAlertEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New site alert %label has been created.', $message_arguments));
        $this->logger('site_alert')->notice('Created new site alert %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The site alert %label has been updated.', $message_arguments));
        $this->logger('site_alert')->notice('Updated site alert %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.site_alert.collection');

    return $result;
  }

}
