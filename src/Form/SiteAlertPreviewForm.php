<?php

namespace Drupal\sitewide_alerts\Form;

use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\sitewide_alerts\SiteAlertInterface;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for previewing site alert.
 */
class SiteAlertPreviewForm extends FormBase {

  /**
   * The site alert entity id.
   */
  protected int $siteAlertId;

  /**
   * The site alert entity.
   */
  protected SiteAlertInterface $siteAlert;

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * Current language code.
   */
  protected string $language;

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * The entity display repository.
   */
  protected EntityDisplayRepository $entityDisplayRepository;

  /**
   * The constructor.
   *
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    SiteAlertService $site_alert_service,
    LanguageManager $language_manager,
    EntityDisplayRepository $entity_display_repository
  ) {
    $this->siteAlertService = $site_alert_service;
    $this->languageManager = $language_manager;
    $this->language = $this->languageManager->getCurrentLanguage()->getId();
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sitewide_alerts.site_alert_service'),
      $container->get('language_manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_alert_preview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $site_alert = NULL): array {
    if (empty($site_alert)) {
      $this->messenger()
        ->addError($this->t('Site alert id is missing or not found. Unable to preview alert.'));
      return $form;
    }

    $this->siteAlertId = (int) $site_alert;
    $this->siteAlert = $this->siteAlertService->getSiteAlert($this->siteAlertId);

    $config = $this->siteAlertService->getConfig();

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Preview settings'),
    ];

    $form['settings']['language'] = [
      '#markup' => '<p>' . $this->t('Language: %language', ['%language' => $this->languageManager->getCurrentLanguage()->getName()]) . '</p>',
    ];

    $view_modes = [
      'default' => $this->t('Default'),
    ];
    foreach ($this->entityDisplayRepository->getViewModes('site_alert') as $id => $view_mode) {
      $view_modes[$id] = $view_mode['label'];
    }

    $view_mode = $config->get('default_view_mode');
    if (!empty($form_state->getValue('view_mode'))) {
      $view_mode = $form_state->getValue('view_mode');
    }

    $form['settings']['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#options' => $view_modes,
      '#description' => $this->t('Select view mode to preview site alert in.'),
      '#default_value' => $view_mode,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::viewModeCallback',
        'wrapper' => 'site-alert-preview',
      ],
      '#required' => TRUE,
    ];

    $form['preview'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Site alert preview'),
      '#description' => $this->t('May appear differently, depending on the theme it will be rendered in.'),
    ];

    $form['preview']['alert'] = $this->siteAlertService->preRenderSiteAlert($this->siteAlert, $view_mode, $this->language);
    $form['preview']['alert']['#prefix'] = '<div id="site-alert-preview" class="c-site-alert__preview">';
    $form['preview']['alert']['#suffix'] = '</div>';

    $form['#attached']['drupalSettings']['sitewide_alerts'] = [
      'dismissedKeys' => [$this->siteAlert->id() . '-' . $this->language],
      'previewMode' => TRUE,
      'cookieExpiration' => $this->siteAlertService->getSiteAlertExpiration(),
    ];

    return $form;
  }

  /**
   * View mode AJAX callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Returns the preview form element.
   */
  public function viewModeCallback(array $form, FormStateInterface $form_state): array {
    return $form['preview']['alert'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->siteAlert)) {
    }
  }

}
