<?php

namespace Drupal\sitewide_alerts\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sitewide_alerts\SiteAlertInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Site alert revision delete form.
 *
 * @package Drupal\sitewide_alerts\Form
 */
class SiteAlertRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The site alert revision.
   */
  protected SiteAlertInterface $revision;

  /**
   * The site alert storage.
   */
  protected EntityStorageInterface $siteAlertStorage;

  /**
   * The database connection.
   */
  protected Connection $connection;

  /**
   * SiteAlertRevisionDeleteForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $entity_storage, Connection $connection) {
    $this->siteAlertStorage = $entity_storage;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('site_alert'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_alert_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $dateFormatter = \Drupal::service('date.formatter');
    return $this->t('Are you sure you want to delete the revision from %revision-date?', ['%revision-date' => $dateFormatter->format($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.site_alert.version_history', ['site_alert' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $site_alert_revision = NULL) {
    $this->revision = $this->siteAlertStorage->loadRevision($site_alert_revision);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dateFormatter = \Drupal::service('date.formatter');

    $this->siteAlertStorage->deleteRevision($this->revision->getRevisionId());
    $this->logger('content')
      ->notice('Site alert: deleted %title revision %revision.', [
        '%title' => $this->revision->label(),
        '%revision' => $this->revision->getRevisionId(),
      ]);

    $this->messenger()->addMessage($this->t('Revision from %revision-date of site alert %title has been deleted.', [
      '%revision-date' => $dateFormatter->format($this->revision->getRevisionCreationTime()),
      '%title' => $this->revision->label(),
    ]));

    $form_state->setRedirect(
      'entity.site_alert.canonical',
      ['popup' => $this->revision->id()]
    );

    if ($this->connection->query('SELECT COUNT(DISTINCT revision_id) FROM {site_alert_field_revision} WHERE id = :id', [':id' => $this->revision->id()])
      ->fetchField() > 1) {

      $form_state->setRedirect(
        'entity.site_alert.version_history',
        ['site_alert' => $this->revision->id()]
      );
    }
  }

}
