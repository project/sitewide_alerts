<?php

namespace Drupal\sitewide_alerts\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Site Alert Settings Form.
 *
 * @package Drupal\sitewide_alerts\Form
 */
class SiteAlertSettingsForm extends ConfigFormBase {

  /**
   * The cache backend.
   */
  protected CacheBackendInterface $cache;

  /**
   * The state.
   */
  protected StateInterface $state;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current language id.
   */
  protected string $language;

  /**
   * The language manager.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entity display repository.
   */
  protected EntityDisplayRepository $entityDisplayRepository;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CacheFactoryInterface $cache_factory,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    EntityDisplayRepository $entity_display_repository
  ) {
    parent::__construct($config_factory);
    $this->cache = $cache_factory->get('render');
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->language = $language_manager->getCurrentLanguage()->getId();
    $this->languageManager = $language_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_factory'),
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'site_alert_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['sitewide_alerts.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sitewide_alerts.settings');

    $form['alert'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Alert Settings <em>(all languages)</em>'),
    ];

    $form['alert']['alert_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable site alerts'),
      '#description' => $this->t('Enable or disable site alerts. When disabled, custom site alert blocks will not appear as well.'),
      '#default_value' => !empty($config->get('active')) ? $config->get('active') : FALSE,
    ];

    $positions = [
      'none' => $this->t('None'),
      'top' => $this->t('Top'),
      'bottom' => $this->t('Bottom'),
    ];
    $form['alert']['alert_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => $positions,
      '#description' => $this->t('Set position of site alert bar when "Alert bar" view mode is used. If block is being used, please select "None" for position.'),
      '#default_value' => !empty($config->get('position')) ? $config->get('position') : 'top',
      '#states' => [
        'visible' => [
          ':input[name="alert_active"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $view_modes = [
      'default' => $this->t('Default'),
    ];
    foreach ($this->entityDisplayRepository->getViewModes('site_alert') as $id => $view_mode) {
      $view_modes[$id] = $view_mode['label'];
    }
    $form['alert']['alert_view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Default view mode'),
      '#options' => $view_modes,
      '#description' => $this->t('Set the default view mode to be used on site alert entities.'),
      '#default_value' => !empty($config->get('default_view_mode')) ? $config->get('default_view_mode') : 'default',
      '#states' => [
        'visible' => [
          ':input[name="alert_active"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $default_alerts_page_node = NULL;
    if (!empty($config->get('default_alerts_page'))) {
      $default_alerts_page_node = $this->entityTypeManager->getStorage('node')->load($config->get('default_alerts_page'));
    }
    $form['alert']['alerts_page'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Default alerts page'),
      '#description' => $this->t('Set the default alerts page where all configured site alerts will appear on.'),
      '#target_type' => 'node',
      '#default_value' => $default_alerts_page_node,
      '#selection_handler' => 'default',
      '#selection_settings' => [
        'target_bundles' => ['page'],
      ],
      '#size' => 40,
      '#states' => [
        'visible' => [
          ':input[name="alert_active"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $expirations = [
      'year' => $this->t('1 Year'),
      'month' => $this->t('1 Month'),
      'week' => $this->t('1 Week'),
      'day' => $this->t('1 Day'),
      'default' => $this->t('Default'),
    ];
    $form['alert']['alert_expiration'] = [
      '#type' => 'select',
      '#title' => $this->t('Cookie expiration'),
      '#options' => $expirations,
      '#description' => $this->t('Set expiration of the site alert cookie.<br>The default value would set the cookie to expire when the browser session ends.'),
      '#default_value' => !empty($config->get('expiration')) ? $config->get('expiration') : 'default',
      '#states' => [
        'visible' => [
          ':input[name="alert_active"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Set config.
    $config = $this->configFactory->getEditable('sitewide_alerts.settings');
    $config->setData([
      'active' => $form_state->getValue('alert_active'),
      'position' => $form_state->getValue('alert_position'),
      'default_view_mode' => $form_state->getValue('alert_view_mode'),
      'default_alerts_page' => $form_state->getValue('alerts_page'),
      'expiration' => $form_state->getValue('alert_expiration'),
    ])->save();

    // Invalidate cache tags.
    Cache::invalidateTags(['sitewide_alerts']);
  }

}
