<?php

namespace Drupal\sitewide_alerts\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for filtering site alerts.
 */
class SiteAlertsFilterForm extends FormBase {

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * The current language code.
   */
  protected string $language;

  /**
   * The constructor.
   *
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   */
  public function __construct(
    SiteAlertService $site_alert_service,
    LanguageManager $language_manager
  ) {
    $this->siteAlertService = $site_alert_service;
    $this->languageManager = $language_manager;
    $this->language = $language_manager->getCurrentLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sitewide_alerts.site_alert_service'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "site_alerts_filter_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['filter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'clearfix'],
      ],
    ];

    $form['filter']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site alert name'),
      '#default_value' => $this->getRequest()->get('name') ?? '',
      '#size' => 30,
    ];

    $form['filter']['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Published status'),
      '#options' => [
        0 => $this->t('Unpublished'),
        1 => $this->t('Published'),
      ],
      '#empty_option' => $this->t('- Any -'),
      '#default_value' => $this->getRequest()->get('status') ?? '',
    ];

    $languages = [];
    foreach ($this->languageManager->getLanguages() as $language) {
      $languages[$language->getId()] = $language->getName();
    }
    $form['filter']['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => $languages,
      '#empty_option' => $this->t('- Any -'),
      '#default_value' => $this->getRequest()->get('language') ?? '',
    ];

    $form['actions']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['form-item']],
    ];

    $form['actions']['wrapper']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    if ($this->getRequest()->getQueryString()) {
      $form['actions']['wrapper']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#submit' => ['::resetForm'],
      ];
    }

    return $form;
  }

  /**
   * Reset form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function resetForm(array $form, FormStateInterface &$form_state): void {
    $form_state->setRedirect('entity.site_alert.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];

    $name = $form_state->getValue('name') ?? '';
    if (!empty($name)) {
      $query['name'] = $name;
    }

    $language = $form_state->getValue('language') ?? '';
    if (!empty($language)) {
      $query['language'] = $language;
    }

    $status = $form_state->getValue('status') ?? '';
    if ($status != '') {
      $query['status'] = $status;
    }

    $form_state->setRedirect('entity.site_alert.collection', $query);
  }

}
