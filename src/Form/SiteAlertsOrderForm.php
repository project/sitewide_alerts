<?php

namespace Drupal\sitewide_alerts\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for ordering site alerts.
 */
class SiteAlertsOrderForm extends FormBase {

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * The current language code.
   */
  protected string $language;

  /**
   * The constructor.
   *
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   */
  public function __construct(
    SiteAlertService $site_alert_service,
    LanguageManager $language_manager
  ) {
    $this->siteAlertService = $site_alert_service;
    $this->languageManager = $language_manager;
    $this->language = $language_manager->getCurrentLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sitewide_alerts.site_alert_service'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "site_alerts_order_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $group_class = 'group-order-weight';

    $site_alerts = $this->siteAlertService->getSiteAlerts($this->language);

    $form['site_alerts'] = [
      '#type' => 'table',
      '#caption' => $this->t('Order site alerts for %language.', ['%language' => $this->languageManager->getCurrentLanguage()->getName()]),
      '#header' => [
        $this->t('ID'),
        $this->t('Label'),
        $this->t('Alert type'),
        $this->t('Language'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('No site alerts found at this time.'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
    ];

    /**
     * @var int $site_alert_id
     * @var \Drupal\sitewide_alerts\Entity\SiteAlert $site_alert
     */
    foreach ($site_alerts as $site_alert_id => $site_alert) {
      $form['site_alerts'][$site_alert_id]['#attributes']['class'][] = 'draggable';
      $form['site_alerts'][$site_alert_id]['#weight'] = $site_alert->getWeight();

      // Site alert id.
      $form['site_alerts'][$site_alert_id]['id'] = [
        '#plain_text' => $site_alert->id(),
      ];

      // Site alert label.
      $form['site_alerts'][$site_alert_id]['label'] = [
        '#plain_text' => $site_alert->label(),
      ];

      // Site alert type.
      $form['site_alerts'][$site_alert_id]['alert_type'] = [
        '#plain_text' => $site_alert->getAlertType() ? $site_alert->getAlertType()->label() : '',
      ];

      // Site alert language.
      $form['site_alerts'][$site_alert_id]['language'] = [
        '#plain_text' => $site_alert->language()->getName(),
      ];

      // Site alert weight.
      $form['site_alerts'][$site_alert_id]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $site_alert->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $site_alert->getWeight(),
        '#attributes' => ['class' => [$group_class]],
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save order'),
      '#disabled' => count($site_alerts) <= 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_alerts = $form_state->getValue('site_alerts');
    foreach ($site_alerts as $site_alert_id => $data) {
      if (!empty($site_alert_id) && isset($data['weight'])) {
        /** @var \Drupal\sitewide_alerts\Entity\SiteAlert $site_alert */
        if ($site_alert = $this->siteAlertService->getSiteAlert($site_alert_id)) {
          // Make sure we are dealing with right translation.
          if ($site_alert->isTranslatable() && $site_alert->hasTranslation($this->language)) {
            $site_alert = $site_alert->getTranslation($this->language);
          }
          // Check current weight against new weight. If not the same, then we
          // will update entity.
          if ($site_alert->getWeight() != $data['weight']) {
            // Don't want to save any revision in this case.
            $site_alert->setNewRevision(FALSE);

            // Set and save new weight.
            $site_alert->set('weight', $data['weight']);
            $site_alert->save();
          }
        }
      }
    }
    $this->messenger()->addMessage($this->t('Site alerts order saved.'));
  }

}
