<?php

namespace Drupal\sitewide_alerts\Form;

use Abraham\TwitterOAuth\TwitterOAuth;
use Abraham\TwitterOAuth\TwitterOAuthException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Url;
use Drupal\key\KeyRepository;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for authorizing Twitter.
 */
class TwitterAuthorizeForm extends FormBase {

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * The current language code.
   */
  protected string $language;

  /**
   * The key repository.
   */
  protected KeyRepository $keyRepository;

  /**
   * The constructor.
   *
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\key\KeyRepository $key_repository
   *   The key repository.
   */
  public function __construct(
    SiteAlertService $site_alert_service,
    LanguageManager $language_manager,
    KeyRepository $key_repository
  ) {
    $this->siteAlertService = $site_alert_service;
    $this->languageManager = $language_manager;
    $this->language = $language_manager->getCurrentLanguage()->getId();
    $this->keyRepository = $key_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sitewide_alerts.site_alert_service'),
      $container->get('language_manager'),
      $container->get('key.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "twitter_authorize_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $site_alert = NULL): array {
    $disabled = FALSE;
    // Get current language and associated twitter configuration.
    $state_keys = [
      'twitter_key_name.' . $this->language,
      'twitter_access_token.' . $this->language,
      'twitter_secret_token.' . $this->language,
    ];
    $config = $this->siteAlertService->getStateConfig($state_keys);

    // Get twitter key name and warning check/message.
    $twitter_key_name = $config['twitter_key_name.' . $this->language] ?? NULL;
    if (empty($twitter_key_name)) {
      $this->messenger()
        ->addWarning($this->t('Twitter app key is not configured. Please configure key on <a href="@link">settings page</a>.', [
          '@link' => Url::fromRoute('sitewide_alerts.twitter_config_form')
            ->toString(),
        ]));
      $disabled = TRUE;
    }

    // Twitter account authorization tokens warning.
    $twitter_access_token = $config['twitter_access_token.' . $this->language] ?? NULL;
    $twitter_secret_token = $config['twitter_secret_token.' . $this->language] ?? NULL;
    if (!empty($twitter_access_token) || !empty($twitter_secret_token)) {
      $this->messenger()
        ->addWarning($this->t('Twitter account authorization tokens found. Generating new authorization will overwrite existing tokens.'));
    }

    $form['auth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authorization <em>(@language)</em>', [
        '@language' => $this->languageManager->getCurrentLanguage()
          ->getName(),
      ]),
      '#disabled' => empty($twitter_key_name),
    ];

    $form['auth']['description'] = [
      '#markup' => '<p>' . $this->t('Generates authorization url which allows app to be associated with Twitter account.') . '</p>',
    ];

    $form['auth']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate Authorization Url'),
      '#disabled' => $disabled,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get current language and associated twitter configuration.
    $state_keys = [
      'twitter_key_name.' . $this->language,
    ];
    $config = $this->siteAlertService->getStateConfig($state_keys);

    // Get twitter key name.
    $twitter_key_name = $config['twitter_key_name.' . $this->language] ?? NULL;
    if (!empty($twitter_key_name)) {

      // Get key and credentials.
      $twitter_credentials = parse_ini_string($this->keyRepository->getKey($twitter_key_name)
        ->getKeyValue());

      // Set twitter configuration vars.
      $twitter_consumer_key = $twitter_credentials['key'] ?? NULL;
      $twitter_consumer_secret = $twitter_credentials['key_secret'] ?? NULL;

      // Get local callback url.
      $oauth_callback = Url::fromRoute('sitewide_alerts.twitter_oauth_callback')
        ->setAbsolute(TRUE)
        ->toString();

      $error = FALSE;

      try {
        $connection = new TwitterOAuth($twitter_consumer_key, $twitter_consumer_secret);
        $response = $connection->oauth("oauth/request_token", ["oauth_callback" => $oauth_callback]);
        $oauth_token = $response['oauth_token'];
        if (!empty($oauth_token)) {
          $authorize_url = $connection->url("oauth/authorize", ["oauth_token" => $oauth_token]);
          if (!empty($authorize_url)) {
            $this->messenger()
              ->addMessage($this->t('Please authorize using the following <a href="@authorize_url" target="_blank">@authorize_url</a>.', ['@authorize_url' => $authorize_url]));
          }
          else {
            $error = TRUE;
          }
        }
        else {
          $error = TRUE;
        }
      }
      catch (TwitterOAuthException $exception) {
        watchdog_exception('sitewide_alerts', $exception);
        $this->messenger()->addError($exception->getMessage());
        $error = TRUE;
      }

      if ($error) {
        $this->messenger()
          ->addError($this->t('Failed to generate authorization url. Please try again.'));
      }
    }
  }

}
