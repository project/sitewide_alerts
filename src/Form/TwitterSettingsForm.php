<?php

namespace Drupal\sitewide_alerts\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Twitter Settings Form.
 *
 * @package Drupal\sitewide_alerts\Form
 */
class TwitterSettingsForm extends ConfigFormBase {

  /**
   * The cache backend.
   */
  protected CacheBackendInterface $cache;

  /**
   * The state.
   */
  protected StateInterface $state;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current language id.
   */
  protected string $language;

  /**
   * The language manager.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entity display repository.
   */
  protected EntityDisplayRepository $entityDisplayRepository;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CacheFactoryInterface $cache_factory,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    EntityDisplayRepository $entity_display_repository
  ) {
    parent::__construct($config_factory);
    $this->cache = $cache_factory->get('render');
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->language = $language_manager->getCurrentLanguage()->getId();
    $this->languageManager = $language_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_factory'),
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'twitter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['sitewide_alerts.settings'];
  }

  /**
   * Get state configuration.
   *
   * @return array
   *   Returns an array of state configuration.
   */
  private function getStateConfig(): array {
    $state_keys = [
      'twitter_key_name.' . $this->language,
      'twitter_access_token.' . $this->language,
      'twitter_secret_token.' . $this->language,
    ];
    return $this->state->getMultiple($state_keys);
  }

  /**
   * Set state configuration data.
   *
   * @param array $data
   *   The array of state config.
   */
  private function setStateConfig(array $data): void {
    $this->state->setMultiple($data);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $state_config = $this->getStateConfig();

    $form['twitter'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Twitter Settings <em>(@language)</em>', [
        '@language' => $this->languageManager->getCurrentLanguage()
          ->getName(),
      ]),
      '#description' => $this->t('Must be configured per language. If using multiple languages, multiple credentials keys will/may be required.'),
    ];

    $key_collection_url = Url::fromRoute('entity.key.collection')->toString();
    $form['twitter']['keys'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Keys'),
      '#description' => $this->t('Use keys managed by the key module. <a href=":keys">Manage keys</a>', [
        ':keys' => $key_collection_url,
      ]),
      '#tree' => FALSE,
    ];

    $form['twitter']['keys']['twitter_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Twitter app credentials key'),
      '#description' => $this->t('Select key to be used for Twitter app credentials.<br>Credentials can be found on the <a href="https://developer.twitter.com/en/portal/dashboard" target="_blank">Twitter developer dashboard</a>.'),
      '#empty_option' => $this->t('- Select Key -'),
      '#default_value' => !empty($state_config['twitter_key_name.' . $this->language]) ? $state_config['twitter_key_name.' . $this->language] : '',
    ];

    $auth_config_url = Url::fromRoute('sitewide_alerts.twitter_authorize_form')
      ->toString();
    $form['twitter']['auth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authorization'),
      '#description' => $this->t('Manage account authorization tokens associated with app. <a href=":auth">Manage authorization</a>.', [':auth' => $auth_config_url]),
      '#tree' => FALSE,
    ];

    $twitter_oauth_access_token = !empty($state_config['twitter_access_token.' . $this->language]) ? $state_config['twitter_access_token.' . $this->language] : '';
    $twitter_oauth_secret_token = !empty($state_config['twitter_secret_token.' . $this->language]) ? $state_config['twitter_secret_token.' . $this->language] : '';
    if (!empty($twitter_oauth_access_token) && !empty($twitter_oauth_secret_token)) {
      $form['twitter']['auth']['twitter_access_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Twitter user access token'),
        '#default_value' => $this->maskToken($twitter_oauth_access_token),
        '#disabled' => TRUE,
      ];
      $form['twitter']['auth']['twitter_secret_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Twitter user secret token'),
        '#default_value' => $this->maskToken($twitter_oauth_secret_token),
        '#disabled' => TRUE,
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Mask token.
   *
   * @param string $token
   *   The token.
   *
   * @return string
   *   The masked token.
   */
  private function maskToken(string $token): string {
    return substr_replace($token, str_repeat('*', strlen($token) - 4), 0, -4);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Set state config.
    $this->setStateConfig([
      'twitter_key_name.' . $this->language => $form_state->getValue('twitter_key_name'),
    ]);

    // Invalidate cache tags.
    Cache::invalidateTags(['sitewide_alerts']);
  }

}
