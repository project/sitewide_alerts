<?php

namespace Drupal\sitewide_alerts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Site Alert Bar Block.
 *
 * @Block(
 *   id = "sitewide_alerts_bar",
 *   admin_label = @Translation("Site Alert Bar"),
 *   category = @Translation("Site Alert"),
 * )
 */
class SiteAlertBarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The admin context.
   */
  protected AdminContext $adminContext;

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * The current language id.
   */
  protected string $language;

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
    ConfigFactoryInterface $config_factory,
    AdminContext $admin_context,
    LanguageManager $language_manager,
    SiteAlertService $site_alert_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->adminContext = $admin_context;
    $this->languageManager = $language_manager;
    $this->language = $this->languageManager->getCurrentLanguage()->getId();
    $this->siteAlertService = $site_alert_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('router.admin_context'),
      $container->get('language_manager'),
      $container->get('sitewide_alerts.site_alert_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->siteAlertService->getSiteAlertBar($this->language);
  }

}
