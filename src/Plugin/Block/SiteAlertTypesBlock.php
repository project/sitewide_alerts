<?php

namespace Drupal\sitewide_alerts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Site Alert Types Block.
 *
 * @Block(
 *   id = "sitewide_alerts_types",
 *   admin_label = @Translation("Site Alert Types"),
 *   category = @Translation("Site Alert"),
 * )
 */
class SiteAlertTypesBlock extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * The config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * The entity display repository.
   */
  protected EntityDisplayRepository $entityDisplayRepository;

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * Current language code.
   */
  protected string $language;

  /**
   * SiteAlertTypesBlock constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   */
  public function __construct(
    array $configuration,
          string $plugin_id,
          mixed $plugin_definition,
    ConfigFactoryInterface $config_factory,
    SiteAlertService $site_alert_service,
    EntityDisplayRepository $entity_display_repository,
    LanguageManager $language_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->siteAlertService = $site_alert_service;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->languageManager = $language_manager;
    $this->language = $this->languageManager->getCurrentLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('sitewide_alerts.site_alert_service'),
      $container->get('entity_display.repository'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => 'default',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $view_modes = [
      'default' => $this->t('Default'),
    ];
    foreach ($this->entityDisplayRepository->getViewModes('taxonomy_term') as $id => $view_mode) {
      $view_modes[$id] = $view_mode['label'];
    }
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Taxonomy term view mode'),
      '#description' => $this->t('Select which view mode to use when rendering site alert type.'),
      '#options' => $view_modes,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => !empty($config['view_mode']) ? $config['view_mode'] : 'default',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['view_mode'] = $values['view_mode'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $view_mode = $this->configuration['view_mode'];

    $build = [
      '#theme' => 'site_alert_types',
      '#view_mode' => $view_mode,
    ];

    // Get site alert types.
    $build['#alert_types'] = [];
    $site_alert_types = [];

    // Make sure site alerts are enabled.
    if ($this->siteAlertService->isEnabled()) {
      $site_alert_types = $this->siteAlertService->getSiteAlertTypes($this->language);
      /** @var \Drupal\taxonomy\Entity\Term $site_alert_type */
      foreach ($site_alert_types as $site_alert_type) {
        $build['#alert_types'][] = $this->siteAlertService->preRenderSiteAlertType($site_alert_type, $view_mode, $this->language);
      }
    }

    $cacheableMetadata = new CacheableMetadata();
    $cacheableMetadata->addCacheableDependency($this->configuration);
    foreach ($site_alert_types as $site_alert_type) {
      $cacheableMetadata->addCacheableDependency($site_alert_type);
    }
    $cacheableMetadata->addCacheTags(['sitewide_alerts']);
    $cacheableMetadata->applyTo($build);

    return $build;
  }

}
