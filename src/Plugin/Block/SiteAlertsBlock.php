<?php

namespace Drupal\sitewide_alerts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sitewide_alerts\SiteAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Site Alerts Block.
 *
 * @Block(
 *   id = "sitewide_alerts",
 *   admin_label = @Translation("Site Alerts"),
 *   category = @Translation("Site Alert"),
 * )
 */
class SiteAlertsBlock extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * The config factory.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The site alert service.
   */
  protected SiteAlertService $siteAlertService;

  /**
   * The entity display repository.
   */
  protected EntityDisplayRepository $entityDisplayRepository;

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * Current language code.
   */
  protected string $language;

  /**
   * SiteAlertsBlock constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\sitewide_alerts\SiteAlertService $site_alert_service
   *   The site alert service.
   * @param \Drupal\Core\Entity\EntityDisplayRepository $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   */
  public function __construct(
    array $configuration,
          string $plugin_id,
          mixed $plugin_definition,
    ConfigFactoryInterface $config_factory,
    SiteAlertService $site_alert_service,
    EntityDisplayRepository $entity_display_repository,
    LanguageManager $language_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->siteAlertService = $site_alert_service;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->languageManager = $language_manager;
    $this->language = $this->languageManager->getCurrentLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('sitewide_alerts.site_alert_service'),
      $container->get('entity_display.repository'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => 'default',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $view_modes = [
      'default' => $this->t('Default'),
    ];
    foreach ($this->entityDisplayRepository->getViewModes('site_alert') as $id => $view_mode) {
      if ($id != 'alert_bar') {
        $view_modes[$id] = $view_mode['label'];
      }
    }
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Site alert view mode'),
      '#description' => $this->t('Select which view mode to use when rendering site alert entities.'),
      '#options' => $view_modes,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => !empty($config['view_mode']) ? $config['view_mode'] : 'default',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['view_mode'] = $values['view_mode'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $view_mode = $this->configuration['view_mode'];

    $build = [
      '#theme' => 'site_alerts',
      '#view_mode' => $view_mode,
    ];

    // Get site alerts.
    $build['#alerts'] = [];
    $site_alerts = [];

    // Make sure site alerts are enabled.
    if ($this->siteAlertService->isEnabled()) {
      $site_alerts = $this->siteAlertService->getSiteAlerts($this->language);
      /** @var \Drupal\sitewide_alerts\SiteAlertInterface $site_alert */
      foreach ($site_alerts as $site_alert) {
        $build['#alerts'][] = $this->siteAlertService->preRenderSiteAlert($site_alert, $view_mode, $this->language);
      }
    }

    $cacheableMetadata = new CacheableMetadata();
    $cacheableMetadata->addCacheableDependency($this->configuration);
    /** @var \Drupal\sitewide_alerts\SiteAlertInterface $site_alert */
    foreach ($site_alerts as $site_alert) {
      $cacheableMetadata->addCacheableDependency($site_alert);
    }
    $cacheableMetadata->addCacheTags(['sitewide_alerts']);
    $cacheableMetadata->applyTo($build);

    return $build;
  }

}
