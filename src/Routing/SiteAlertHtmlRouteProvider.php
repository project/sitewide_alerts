<?php

namespace Drupal\sitewide_alerts\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class SiteAlertHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($preview_route = $this->getPreviewRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.preview", $preview_route);
    }

    if ($tweet_route = $this->getTweetRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.tweet", $tweet_route);
    }

    if ($history_route = $this->getHistoryRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.version_history", $history_route);
    }

    if ($revision_route = $this->getRevisionRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision", $revision_route);
    }

    if ($revert_route = $this->getRevisionRevertRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision_revert", $revert_route);
    }

    if ($delete_route = $this->getRevisionDeleteRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision_delete", $delete_route);
    }

    if ($translation_route = $this->getRevisionTranslationRevertRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision_revert_translation", $translation_route);
    }

    return $collection;
  }

  /**
   * Gets the site alert preview route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getPreviewRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('preview-form')) {
      $route = new Route($entity_type->getLinkTemplate('preview-form'));
      $route
        ->setDefaults([
          '_form' => '\Drupal\sitewide_alerts\Form\SiteAlertPreviewForm',
          '_title' => 'Preview alert',
        ])
        ->setRequirement('_permission', 'view site alert')
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * Gets the site alert tweet route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getTweetRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('tweet-form')) {
      $route = new Route($entity_type->getLinkTemplate('tweet-form'));
      $route
        ->setDefaults([
          '_form' => '\Drupal\sitewide_alerts\Form\SiteAlertTweetForm',
          '_title' => 'Tweet alert',
        ])
        ->setRequirement('_permission', 'tweet site alert')
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * Gets the version history route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getHistoryRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('version-history')) {
      $route = new Route($entity_type->getLinkTemplate('version-history'));
      $route
        ->setDefaults([
          '_title' => "{$entity_type->getLabel()} revisions",
          '_controller' => '\Drupal\sitewide_alerts\Controller\SiteAlertController::revisionOverview',
        ])
        ->setRequirement('_permission', 'access site alert revisions')
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * Gets the revision route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('revision')) {
      $route = new Route($entity_type->getLinkTemplate('revision'));
      $route
        ->setDefaults([
          '_controller' => '\Drupal\sitewide_alerts\Controller\SiteAlertController::revisionShow',
          '_title_callback' => '\Drupal\sitewide_alerts\Controller\SiteAlertController::revisionPageTitle',
        ])
        ->setRequirement('_permission', 'access site alert revisions')
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * Gets the revision revert route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionRevertRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('revision-revert')) {
      $route = new Route($entity_type->getLinkTemplate('revision-revert'));
      $route
        ->setDefaults([
          '_form' => '\Drupal\sitewide_alerts\Form\SiteAlertRevisionRevertForm',
          '_title' => 'Revert to earlier revision',
        ])
        ->setRequirement('_permission', 'revert all site alert revisions')
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * Gets the revision delete route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionDeleteRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('revision-delete')) {
      $route = new Route($entity_type->getLinkTemplate('revision-delete'));
      $route
        ->setDefaults([
          '_form' => '\Drupal\sitewide_alerts\Form\SiteAlertRevisionDeleteForm',
          '_title' => 'Delete earlier revision',
        ])
        ->setRequirement('_permission', 'delete all site alert revisions')
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * Gets the revision translation revert route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionTranslationRevertRoute(EntityTypeInterface $entity_type): ?Route {
    if ($entity_type->hasLinkTemplate('translation-revert')) {
      $route = new Route($entity_type->getLinkTemplate('translation-revert'));
      $route
        ->setDefaults([
          '_form' => '\Drupal\sitewide_alerts\Form\SiteAlertRevisionRevertTranslationForm',
          '_title' => 'Revert to earlier revision of a translation',
        ])
        ->setRequirement('_permission', 'revert all site alert revisions')
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    return $this->getEditFormRoute($entity_type);
  }

}
