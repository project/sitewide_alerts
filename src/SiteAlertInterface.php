<?php

namespace Drupal\sitewide_alerts;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a site alert entity type.
 */
interface SiteAlertInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Get the alert weight.
   *
   * @return int|null
   *   Returns the weight.
   */
  public function getWeight(): ?int;

  /**
   * Get the alert type.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns the associated taxonomy term entity.
   */
  public function getAlertType(): ?EntityInterface;

  /**
   * Get the alert type icon image.
   *
   * @return array
   *   Returns array of information for icon.
   */
  public function getAlertTypeIcon(): array;

  /**
   * Get the alert type color.
   *
   * @return string
   *   Returns the hex code for color.
   */
  public function getAlertTypeColor(): string;

  /**
   * Get the alert link.
   *
   * @return string
   *   Returns the alert link.
   */
  public function getAlertLink(): string;

  /**
   * Check to see if site alert is dismissible.
   *
   * @return bool
   *   Returns TRUE if dismissible, FALSE otherwise.
   */
  public function isDismissible(): bool;

}
