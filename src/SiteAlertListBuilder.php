<?php

namespace Drupal\sitewide_alerts;

use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a list controller for the site alert entity type.
 */
class SiteAlertListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The request stack.
   */
  protected RequestStack $requestStack;

  /**
   * The form builder.
   */
  protected FormBuilder $formBuilder;

  /**
   * Constructs a new SiteAlertListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Form\FormBuilder $form_builder
   *   The form builder.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    DateFormatterInterface $date_formatter,
    RequestStack $request_stack,
    FormBuilder $form_builder
  ) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('request_stack'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $connection = Database::getConnection();
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->entityType->getKey('id'));

    // Get current request.
    $request = $this->requestStack->getCurrentRequest();

    // Filter by name.
    $name = $request->get('name') ?? '';
    if (!empty($name)) {
      $query->condition('label', '%' . $connection->escapeLike($name) . '%', 'LIKE');
    }

    // Filter by language.
    $language = $request->get('language') ?? '';
    if (!empty($language)) {
      $query->condition('langcode', $language, '=');
    }

    // Filter by status.
    $status = $request->get('status') ?? '';
    if ($status != '') {
      $query->condition('status', $status, '=');
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['form'] = $this->formBuilder->getForm('Drupal\sitewide_alerts\Form\SiteAlertsFilterForm');
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];

    // Get current request.
    $request = $this->requestStack->getCurrentRequest();
    // Get filtered language, if set.
    $filtered_language = $request->get('language') ?? '';

    /** @var \Drupal\sitewide_alerts\Entity\SiteAlert $entity */
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        if (empty($filtered_language) || $entity->language()->getId() == $filtered_language) {
          $build['table']['#rows'][$entity->id() . '-' . $entity->language()
            ->getId()] = $row;
        }
      }
      if ($languages = $entity->getTranslationLanguages(FALSE)) {
        foreach ($languages as $language) {
          if ($entity->hasTranslation($language->getId())) {
            $entity = $entity->getTranslation($language->getId());
            if ($row = $this->buildRow($entity)) {
              if (empty($filtered_language) || $entity->language()->getId() == $filtered_language) {
                $build['table']['#rows'][$entity->id() . '-' . $entity->language()
                  ->getId()] = $row;
              }
            }
          }
        }
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['alert_type'] = $this->t('Alert type');
    $header['language'] = $this->t('Language');
    $header['status'] = $this->t('Status');
    $header['changed'] = $this->t('Last updated');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\sitewide_alerts\SiteAlertInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['alert_type'] = $entity->getAlertType() ? $entity->getAlertType()->label() : '';
    $row['language'] = $entity->language()->getName();
    $row['status'] = $entity->get('status')->value ? $this->t('Published') : $this->t('Unpublished');
    $row['changed'] = $entity->getChangedTime() ? $this->dateFormatter->format($entity->getChangedTime()) : '';

    return $row + parent::buildRow($entity);
  }

}
