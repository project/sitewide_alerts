<?php

namespace Drupal\sitewide_alerts;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Site alert storage.
 *
 * @package Drupal\sitewide_alerts
 */
class SiteAlertStorage extends SqlContentEntityStorage implements SiteAlertStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(SiteAlertInterface $entity): array {
    return $this->database->query(
      'SELECT revision_id FROM {site_alert_revision} WHERE id=:id ORDER BY revision_id',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account): array {
    return $this->database->query(
      'SELECT revision_id FROM {site_alert_field_revision} WHERE uid = :uid ORDER BY revision_id',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(SiteAlertInterface $entity): int {
    return $this->database->query('SELECT COUNT(*) FROM {site_alert_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language): void {
    $this->database->update('site_alert_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
