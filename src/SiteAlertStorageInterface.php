<?php

namespace Drupal\sitewide_alerts;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Site alert storage interface.
 *
 * @package Drupal\sitewide_alerts
 */
interface SiteAlertStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of revision IDs for a specific site alert.
   *
   * @param \Drupal\sitewide_alerts\SiteAlertInterface $entity
   *   The site alert entity.
   *
   * @return int[]
   *   Returns the revision IDs (in ascending order).
   */
  public function revisionIds(SiteAlertInterface $entity): array;

  /**
   * Gets a list of revision IDs having a given user as site alert author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Returns the revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account): array;

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\sitewide_alerts\SiteAlertInterface $entity
   *   The site alert entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(SiteAlertInterface $entity): int;

  /**
   * Unsets the language for all site alerts with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language): void;

}
