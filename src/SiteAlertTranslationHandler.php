<?php

namespace Drupal\sitewide_alerts;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Site alert translation handler.
 *
 * @package Drupal\sitewide_alerts
 */
class SiteAlertTranslationHandler extends ContentTranslationHandler {
  // Override here the needed methods from ContentTranslationHandler.
}
